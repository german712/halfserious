import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Starship } from '../../models/starship.model';

@Component({
  selector: 'half-list-element',
  templateUrl: './list-element.component.html',
  styleUrls: ['./list-element.component.scss'],
})
export class ListElementComponent implements OnInit {
  @Input() starship: Starship;
  @Output() selectedStarship: EventEmitter<Starship> =
    new EventEmitter<Starship>();
  constructor() {}

  ngOnInit(): void {}
  selectStarship() {
    this.selectedStarship.emit(this.starship);
  }
}
