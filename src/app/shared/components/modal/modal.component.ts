import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StarshipService } from 'src/app/core/services/starship.service';
import { Pilot, Starship } from '../../models/starship.model';

@Component({
  selector: 'half-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  starship: Starship;

  constructor(
    private starshipService: StarshipService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.starshipService.starshipSelected.subscribe(
      (starship) => (this.starship = starship)
    );
  }

  selectPilot(pilot: Pilot) {
    this.starshipService.selectPilot(pilot);
    this.router.navigate(['pilot']);
  }
}
