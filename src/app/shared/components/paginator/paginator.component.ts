import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'half-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent implements OnInit {
  pages: number;
  @Output() changePage: EventEmitter<number> = new EventEmitter<number>();
  activePage: number = 1;
  constructor() {}

  ngOnInit(): void {
    this.pages = Number(localStorage.getItem('pages'));
  }

  goTo(page: number) {
    this.activePage = page;
    this.changePage.emit(page);
  }
}
