import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ListElementComponent } from './components/list-element/list-element.component';
import { ModalComponent } from './components/modal/modal.component';
import { PaginatorComponent } from './components/paginator/paginator.component';

@NgModule({
  declarations: [ListElementComponent, PaginatorComponent, ModalComponent],
  imports: [CommonModule],
  exports: [ListElementComponent, PaginatorComponent, ModalComponent],
})
export class SharedModule {}
