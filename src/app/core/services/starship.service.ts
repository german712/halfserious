import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pilot, Starship } from 'src/app/shared/models/starship.model';

@Injectable({
  providedIn: 'root',
})
export class StarshipService {
  starshipSelected: Subject<Starship> = new Subject<Starship>();
  pilotSelected: Subject<Pilot> = new Subject<Pilot>();
  pilot: Pilot;
  constructor(private http: HttpClient) {
    this.pilotSelected.subscribe((pilot) => (this.pilot = pilot));
  }

  getStarships(page: number): Observable<DataResponse> {
    return this.http
      .get('https://swapi.dev/api/starships/?page=' + page)
      .pipe(map((data) => this.formatData(data)));
  }

  formatData(data: any) {
    return { starships: data.results, total: data.count };
  }

  selectStarship(starship: Starship) {
    const newStarship = { ...starship, pilotsData: [] as Pilot[] };
    newStarship.pilots.forEach((pilot) =>
      this.getPilot(pilot).subscribe((pilotData) =>
        newStarship.pilotsData?.push(pilotData)
      )
    );
    this.starshipSelected.next(newStarship);
  }

  getPilot(pilot: any): Observable<Pilot> {
    return this.http
      .get(pilot)
      .pipe(map((pilotData) => this.formatPilot(pilotData)));
  }

  formatPilot(pilotData: any): Pilot {
    return pilotData;
  }

  selectPilot(pilot: Pilot) {
    this.pilotSelected.next(pilot);
  }
}

export interface DataResponse {
  total: number;
  starships: Starship[];
}
