import { createAction, props } from '@ngrx/store';
import { Starship } from 'src/app/shared/models/starship.model';

export const retrievedStarshipList = createAction(
  '[Starship List/API] Retrieve Starships Success',
  props<{ page: number; starships: Starship[] }>()
);
