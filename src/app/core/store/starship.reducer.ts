import { createReducer, on } from '@ngrx/store';
import { StarshipPage } from 'src/app/shared/models/starship.model';
import { retrievedStarshipList } from './starships.actions';

export const initialState: ReadonlyArray<StarshipPage> = [];
export const initialState2: StarshipPage = { page: 1, starships: [] };

export const starshipsPagesReducer = createReducer(
  initialState,
  on(retrievedStarshipList, (state, { page, starships }) => {
    if (state.find((starshipPage) => starshipPage.page === page)) {
      return { ...state };
    }
    return state.concat([{ page: page, starships: starships }]);
  })
);
