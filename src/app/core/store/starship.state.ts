import { StarshipPage } from 'src/app/shared/models/starship.model';

export interface AppState {
  starshipsPages: ReadonlyArray<StarshipPage>;
}
