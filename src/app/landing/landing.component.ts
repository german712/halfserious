import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  DataResponse,
  StarshipService,
} from 'src/app/core/services/starship.service';
import { retrievedStarshipList } from 'src/app/core/store/starships.actions';
import { Starship, StarshipPage } from 'src/app/shared/models/starship.model';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {
  starships: StarshipPage;
  starshipsPages: StarshipPage[];
  pages: number;
  constructor(
    private starshipService: StarshipService,
    private store: Store<any>
  ) {}

  ngOnInit() {
    this.store.select('starshipsPages').subscribe((starshipsPages) => {
      this.starshipsPages = starshipsPages;
    });
    this.updateSelectedPage(1);
  }
  updateSelectedPage(page: number) {
    const selectedPage = this.starshipsPages?.find(
      (starshipsPage) => starshipsPage.page === page
    );
    if (selectedPage) {
      this.starships = selectedPage;
    } else {
      this.updateData(page);
    }
  }
  updateData(page: number) {
    this.starshipService.getStarships(page).subscribe((data: DataResponse) => {
      this.pages = Math.ceil(data.total / 10);
      localStorage.setItem('pages', this.pages.toString());
      this.store.dispatch(
        retrievedStarshipList({ page, starships: data.starships })
      );
      this.updateSelectedPage(page);
    });
  }
  goTo(page: number) {
    this.updateSelectedPage(page);
  }
  selectStarship(starship: Starship) {
    this.starshipService.selectStarship(starship);
  }
}
