import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PilotsComponent } from './pilots.component';
import { PilotsRoutingModule } from './pilots.routing';

@NgModule({
  declarations: [PilotsComponent],
  imports: [CommonModule, PilotsRoutingModule],
})
export class PilotsModule {}
