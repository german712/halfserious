import { Component, OnInit } from '@angular/core';
import { StarshipService } from '../core/services/starship.service';

@Component({
  selector: 'app-pilots',
  templateUrl: './pilots.component.html',
  styleUrls: ['./pilots.component.scss'],
})
export class PilotsComponent implements OnInit {
  constructor(public starshipService: StarshipService) {}

  ngOnInit(): void {}
}
